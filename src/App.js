import './App.css';
import  { Fragment } from 'react';

import AppNavBar from './components/AppNavBar';
import Home from './pages/Home'

function App() {
  return (
    <Fragment>
      <AppNavBar />
      <Home />
    </Fragment>
  );
}

export default App;
