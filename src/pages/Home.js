import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Courses from '../components/Courses';
import  { Fragment } from 'react';

function Home() {
    return (
        <Fragment>
            <Banner />
            <Highlights />
            <Courses />
        </Fragment>
    );
}

export default Home;