import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import { Container, Row, Col } from 'react-bootstrap';

function Banner() {
    return(
        <Container>
            <Row className="justify-content-center">
                <Col xs={10} md={8}>
                    <Jumbotron>
                        <h1>Welcome to React-Booking App!</h1>
                        <p>
                            Opportunities for everyone, everywhere!
                        </p>
                        <p>
                            <Button variant="primary">Enroll</Button>
                        </p>
                    </Jumbotron>
                </Col>
            </Row>
        </Container>
    );
}

export default Banner;