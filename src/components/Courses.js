import { Card, Container, Row, Col, Button } from 'react-bootstrap';

function Courses() {
    return(
        <Container fluid className="mb-4">
            <Row>
                {/* Card 1 */}
                <Col xs={12} md={2}>
                    <Card className="cardHighlights p-3">
                        <Card.Body>
                            <Card.Title>Sample Course</Card.Title>
                            <Card.Text className="font-weight-bold">Description:</Card.Text>
                            <Card.Text className="">This is a sample course offering</Card.Text>
                            <Card.Text className="font-weight-bold">Price:</Card.Text>
                            <Card.Text>PHP 40,000</Card.Text>
                            <Button variant="primary">Enroll</Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default Courses;